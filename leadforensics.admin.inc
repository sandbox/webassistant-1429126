<?php

/**
 * @file
 * Adds a settings form so admin users can add their Lead Forensics ID.
 */

/**
 * Form: Lead Forensics settings.
 */
function leadforensics_admin_config_form($form, &$form_state) {
  $form['lead_forensics'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['lead_forensics']['leadforensics_id'] = array(
    '#title' => t('Tracker ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('leadforensics_id', ''),
    '#maxlength' => 10,
    '#size' => 10,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
