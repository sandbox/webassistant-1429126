
Lead Forensics Module Readme
----------------------------


Installation
------------

To install this module, place it in your sites/all/modules folder and enable it
on the modules page.


Configuration
-------------

You can visit the configuration page directly at 
admin/config/system/leadforensics to add your Lead Forensics id and start 
tracking.
